package main;

import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class BezierGUI {
	
	public enum State {
		regular,
		drag
	}
	
	public State currentState = State.regular;
	private JFrame mainFrame = new JFrame();
	private JPanel mainPanel = new JPanel();
	private DrawPanel curvePanel = new DrawPanel();
	private JButton clearButton = new JButton("Clear control points");
	Point2D selection = null;
	
	public BezierGUI() {
		clearButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				curvePanel.clearControlPoints();
			}
		});
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		mainFrame.add(mainPanel);
		mainPanel.add(curvePanel);
		mainPanel.add(clearButton);
		mainFrame.setSize(800, 600);
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		curvePanel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				if(currentState == State.regular) {
					addPoint(arg0.getPoint());
				} else if(currentState == State.drag) {
					selection.setLocation(arg0.getPoint());
					curvePanel.calcCurvePoints();
				}
				curvePanel.setDrawSelectionFrame(false);
				curvePanel.repaint();
				currentState = State.regular;
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				selection = curvePanel.getSelectedPoint(arg0.getPoint());
				if (selection != null) {
					currentState = State.drag;
				}
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}

		});
		
		curvePanel.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent arg0) {
				selection = curvePanel.getSelectedPoint(arg0.getPoint());
				if (selection != null) {
					curvePanel.setSelectionFramePosition(selection);
					boolean oldDrawSelectionFrame = curvePanel.isDrawSelectionFrame();
					curvePanel.setDrawSelectionFrame(true);
					if (curvePanel.isDrawSelectionFrame() && oldDrawSelectionFrame) {
						curvePanel.repaint();
					}
				} else {
					if (curvePanel.isDrawSelectionFrame()) {
						curvePanel.setDrawSelectionFrame(false);
						curvePanel.repaint();
					}
				}
			}
			
			@Override
			public void mouseDragged(MouseEvent arg0) {
			}
		});
	}

	private void addPoint(Point point) {
		curvePanel.addPoint(point);
	}
	
	public static void main(String[] args) {
		new BezierGUI();
	}

}
