package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Panel;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import main.BSplineGUI.State;

import org.apache.commons.math3.util.ArithmeticUtils;
import org.junit.Test;

public class DrawPanel extends Panel {
	private static final int dragTolerance = 20;
	private static final int controlPointSize = 10;
	private static final int k = 3;

	private List<Point2D> controlPoints = new ArrayList<>();
	private List<Point2D> curvePoints = new ArrayList<>();
	private Rectangle selectionFrame = new Rectangle(20,20);
	private boolean drawSelectionFrame = false;
	private Point2D draggedPoint = null;
	
	public DrawPanel() {
		super();
	}

	@Override
	public void paint(Graphics g) {
		
		g.setColor(Color.BLACK);
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		g.drawString(controlPoints.size() + " / 20", 5, 20);
		
		for (Point2D point : controlPoints) {
			g.fillArc((int) point.getX()-controlPointSize/2, (int) point.getY()-controlPointSize/2, controlPointSize, controlPointSize, 0, 360);
			g.drawString((int) point.getX() + " | " + (int) point.getY(), (int) point.getX()+10, (int) point.getY()+5);
		}
		
		for (int i = 0; i < curvePoints.size() - 1; i++) {

			g.drawLine((int)curvePoints.get(i).getX(), (int)curvePoints.get(i).getY(), (int)curvePoints.get(i + 1).getX(), (int)curvePoints.get(i + 1).getY());
		}

		g.setColor(Color.RED);
		if (drawSelectionFrame) {
			g.drawRect(selectionFrame.x-selectionFrame.width/2, selectionFrame.y-selectionFrame.height/2, selectionFrame.width, selectionFrame.height);
		}
		drawCoordinateSystem(g);
	}
	private void drawCoordinateSystem(Graphics g) {
		g.drawLine(5, 5, this.getWidth(), 5);
		g.drawLine(5, 5, 5, this.getHeight());
		
		int currentPos = 0;
		while(currentPos < this.getWidth()) {
			g.drawLine(currentPos, 5, currentPos, 10);
			g.drawString("" + currentPos, currentPos, 25);
			g.drawLine(5, currentPos, 10, currentPos);
			g.drawString("" + currentPos, 25, currentPos);
			currentPos += 100;
		}
	}
	
	public void addPoint(Point point) {
		if(controlPoints.size() >= 20) {
			System.out.println("No more points to prevent arithmetic exceptions...");
			return;
		}
		
		// if the last point is added, we remove the last k - 1 points and add the last point k times.
		if (controlPoints.size() > 3) {
			for(int i = 0; i < k - 1; i++) {
				controlPoints.remove(controlPoints.size()-1 - i);
			}
		}
		
		for(int i = 0; i < k; i++) {
			controlPoints.add(point);
		}
		
		calcCurvePoints();
		this.repaint();
	}
	
	public Point2D getSelectedPoint(Point mousePosition) {
		for (Point2D point : controlPoints) {
			if (pointInSelection(point, mousePosition)) {
				return point;
			}
		}
		return null;
	}
	
	public boolean pointInSelection(Point2D point, Point mousePosition) {
		if(epsilonEqual(point.getX(), mousePosition.getX(), dragTolerance) && epsilonEqual(point.getY(), mousePosition.getY(), dragTolerance)) {
			return true;
		}
		return false;
	}

	public static boolean epsilonEqual(double x, double reference, int tolerance) {
		if (x >= reference-tolerance && x<= reference+tolerance) {
			return true;
		}
		return false;
	}
	
	public void setSelectionFramePosition(Point2D selection) {
		setSelectionFramePosition((int)selection.getX(), (int)selection.getY());
	}

	public void setSelectionFramePosition(int x, int y) {
		selectionFrame.x = x;
		selectionFrame.y = y;
	}

	// Calculates the points of the corresponding curve with Bernstein poylnoms.
	// The first for-loop iterates over the interval k [0-1] in which the
	// curve is defined.
	public void calcCurvePoints() {
		curvePoints = new ArrayList<>();

		int n = controlPoints.size() - 1;
		if (n < k) {
			return;
		}
		
		for (float width = 0; width <= 1000; width += 1) {
			float t = width;
			Point newPoint = new Point();
			newPoint.x = (int) width;
			
			for(int i = 0; i < n-k; i++) {
				
				newPoint.y += controlPoints.get(i).getY() * N(i, k, t);
			}
			curvePoints.add(newPoint);
		}
		System.out.println(curvePoints);
	}
	
	private float N(int i, int k, float currentT) {
		float t0 = (float) (controlPoints.get(i).getX());
		float t1 = (float) (controlPoints.get(i+1).getX());
		if (k == 1) {
			if (between(currentT, t0, t1)) {
				//System.out.println(currentT + " is between " + t0 + " und " + t1);
				return 1.0f;
			} else {
				return 0;
			}
		} else {
			float tipluskminus1 = (float) (controlPoints.get(i+k-1).getX());
			float tiplusk = (float) (controlPoints.get(i+k).getX());
			double result = 0.0f;
			result += N(i, k-1, currentT) * (currentT - t0) / (tipluskminus1 - t0 - 0.00000001f);
			result += N(i+1, k-1, currentT) * (tiplusk - currentT) / (tiplusk - t1 - 0.00000001f);
			System.out.println("..result " + result);
			return (float) (result);
		}
	}
	
	private boolean between(float value, float lower, float upper) {
		return (value >= lower && value < upper);
	}
	
	@Test
	public void betweenValues() {
		Assert.assertTrue(between(2.01f, 2, 3));
		Assert.assertTrue(between(2, 2, 3));
		Assert.assertFalse(between(1, 2, 3));
		Assert.assertFalse(between(3, 2, 3));
	}

	public Point2D getDraggedPoint() {
		return draggedPoint;
	}

	public void setDraggedPoint(Point2D draggedPoint) {
		this.draggedPoint = draggedPoint;
	}

	public void clearControlPoints() {
		controlPoints = new ArrayList<>();
		curvePoints = new ArrayList<>();
		repaint();
		
	}

	public boolean isDrawSelectionFrame() {
		return drawSelectionFrame;
	}

	public void setDrawSelectionFrame(boolean drawSelectionFrame) {
		this.drawSelectionFrame = drawSelectionFrame;
	}
	
}
