package test;

import java.awt.Point;
import java.awt.geom.Point2D;

import junit.framework.Assert;

import main.DrawPanel;

import org.junit.Test;
import static main.DrawPanel.epsilonEqual;

public class AllTestSuite {
	
	@Test
	public void epsilonCompare() {
		Assert.assertTrue(epsilonEqual(10.0f, 11.0f, 1));
		Assert.assertFalse(epsilonEqual(10.0f, 12.0f, 1));
	}

	@Test
	public void pointInSelection() {
		DrawPanel panel = new DrawPanel();
		Point addedPoint = new Point(10,10);
		Point2D point = new Point(10,10);
		Assert.assertTrue(panel.pointInSelection(point, addedPoint));
	}
	@Test
	public void selection() {
		DrawPanel panel = new DrawPanel();
		Point addedPoint = new Point(10,10);
		panel.addPoint(addedPoint);
		Point2D point = panel.getSelectedPoint(new Point(10,10));
		Assert.assertEquals(addedPoint.getX(), point.getX());
		Assert.assertEquals(addedPoint.getY(), point.getY());
	}
	
}
